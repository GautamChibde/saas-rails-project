class HomeController < ApplicationController
  skip_before_action :authenticate_tenant!, :only => [:index]

  def index
    if current_user
      if session[:tenant_id]
        Tenant.set_current_tenant session[:tenant_id]
      else
        Tenant.set_current_tenant current_user.tenants.first
      end

      @tenant = Tenant.current_tenant
      @projects = Project.by_plan_and_tenant(@tenant.id)
      params[:tenant_id] = @tenant.id

    end
  end

  def after_confirmation_path_for(resource_name, resource)

    if user_signed_in?

      root_path

    else

      new_user_session_path

    end

  end

  private

  def set_confirmable()
    @confirmable = User.find_or_initialize_with_error_by(:confirmation_token, params[:confirmation_token])
  end
end
